﻿# TODO: Translation updated at 2017-10-07 10:37

translate german strings:

    # screens.rpy:253
    old "Back"
    new "Zurück"

    # screens.rpy:254
    old "History"
    new "Historie"

    # screens.rpy:255
    old "Skip"
    new "Überspringen"

    # screens.rpy:256
    old "Auto"
    new "Automatisch"

    # screens.rpy:257
    old "Save"
    new "Speichern"

    # screens.rpy:258
    old "Q.save"
    new "S.Speichern"

    # screens.rpy:259
    old "Q.load"
    new "S.Laden"

    # screens.rpy:260
    old "Settings"
    new "Einstellungen"

    # screens.rpy:302
    old "New game"
    new "Neues Spiel"

    # screens.rpy:310
    old "Load"
    new "Laden"

    # screens.rpy:313
    old "Bonus"
    new "Bonus"

    # screens.rpy:319
    old "End Replay"
    new "Wiederholung beenden"

    # screens.rpy:323
    old "Main menu"
    new "Hauptmenü"

    # screens.rpy:325
    old "About"
    new "Über"

    # screens.rpy:327
    old "Please donate!"
    new "Bitte Spenden!"

    # screens.rpy:332
    old "Help"
    new "Hilfe"

    # screens.rpy:336
    old "Quit"
    new "Beenden"

    # screens.rpy:387
    old "Update Available!"
    new "Update verfügbar!"

    # screens.rpy:562
    old "Version [config.version!t]\n"
    new "Version [config.version!t]\n"

    # screens.rpy:568
    old "Ren'Py version: [renpy.version_only].\n\n[renpy.license!t]"
    new "Ren'Py version: [renpy.version_only].\n\n[renpy.license!t]"

    # screens.rpy:608
    old "Page {}"
    new "Seite {}"

    # screens.rpy:608
    old "Automatic saves"
    new "Automatische Spielstände"

    # screens.rpy:608
    old "Quick saves"
    new "Schnelle Spielstände"

    # screens.rpy:650
    old "{#file_time}%A, %B %d %Y, %H:%M"
    new "{#file_time}%A, %B %d %Y, %H:%M"

    # screens.rpy:650
    old "empty slot"
    new "LEER"

    # screens.rpy:667
    old "<"
    new "<"

    # screens.rpy:670
    old "{#auto_page}A"
    new "{#auto_page}A"

    # screens.rpy:673
    old "{#quick_page}Q"
    new "{#quick_page}Q"

    # screens.rpy:679
    old ">"
    new ">"

    # screens.rpy:741
    old "Display"
    new "Bildschirm"

    # screens.rpy:742
    old "Windowed"
    new "Fenster"

    # screens.rpy:743
    old "Fullscreen"
    new "Vollbild"

    # screens.rpy:747
    old "Rollback Side"
    new "Seite zurückblättern"

    # screens.rpy:748
    old "Disable"
    new "Deaktivieren"

    # screens.rpy:749
    old "Left"
    new "Links"

    # screens.rpy:750
    old "Right"
    new "Rechts"

    # screens.rpy:755
    old "Unseen Text"
    new "Ungelesener Text"

    # screens.rpy:756
    old "After choices"
    new "Nachwahlmöglichkeiten"

    # screens.rpy:757
    old "Transitions"
    new "Übergänge"

    # screens.rpy:772
    old "Text speed"
    new "Textgeschwindigkeit"

    # screens.rpy:776
    old "Auto forward"
    new "Automatisches Vorblättern"

    # screens.rpy:783
    old "Music volume"
    new "Musik Lautstärke"

    # screens.rpy:790
    old "Sound volume"
    new "Geräusch Lautstärke"

    # screens.rpy:796
    old "Test"
    new "Теst"

    # screens.rpy:800
    old "Voice volume"
    new "Stimmen Lautstärke"

    # screens.rpy:811
    old "Mute All"
    new "Stumm stellen"

    # screens.rpy:822
    old "Language"
    new "Sprache"

    # screens.rpy:948
    old "The dialogue history is empty."
    new "Die Dialoghistorie ist leer."

    # screens.rpy:1013
    old "Keyboard"
    new "Tastatur"

    # screens.rpy:1014
    old "Mouse"
    new "Maus"

    # screens.rpy:1017
    old "Gamepad"
    new "Gamepad"

    # screens.rpy:1030
    old "Enter"
    new "Enter"

    # screens.rpy:1031
    old "Advances dialogue and activates the interface."
    new "Erweitert die Dialoge und aktiviert die Benutzeroberfläche."

    # screens.rpy:1034
    old "Space"
    new "Leertaste"

    # screens.rpy:1035
    old "Advances dialogue without selecting choices."
    new "Erweitert Dialoge ohne eine Auswahl zu treffen."

    # screens.rpy:1038
    old "Arrow Keys"
    new "Pfeiltasten"

    # screens.rpy:1039
    old "Navigate the interface."
    new "Navigiert durch die Benutzeroberfläche"

    # screens.rpy:1042
    old "Escape"
    new "Escape"

    # screens.rpy:1043
    old "Accesses the game menu."
    new "Ruft das Spielmenü auf."

    # screens.rpy:1046
    old "Ctrl"
    new "Ctrl"

    # screens.rpy:1047
    old "Skips dialogue while held down."
    new "Überspringt Dialoge beim halten der unteren Pfeiltaste."

    # screens.rpy:1050
    old "Tab"
    new "Tab"

    # screens.rpy:1051
    old "Toggles dialogue skipping."
    new "Schaltet das Dialogüberspringen ein."

    # screens.rpy:1054
    old "Page Up"
    new "Seite nach oben"

    # screens.rpy:1055
    old "Rolls back to earlier dialogue."
    new "Blättert zum vorherigen Dialog zurück."

    # screens.rpy:1058
    old "Page Down"
    new "Seite nach unten"

    # screens.rpy:1059
    old "Rolls forward to later dialogue."
    new "Blättert zum nächsten Dialog vor."

    # screens.rpy:1063
    old "Hides the user interface."
    new "Verbergt die Benutzeroberfläche."

    # screens.rpy:1067
    old "Takes a screenshot."
    new "Macht ein Screenshot."

    # screens.rpy:1071
    old "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."
    new "Schaltet das assisitive {a=https://www.renpy.org/l/voicing}Self-voicing{/a}. ein"

    # screens.rpy:1077
    old "Left Click"
    new "Linksklick"

    # screens.rpy:1081
    old "Middle Click"
    new "Mittlere Maustaste"

    # screens.rpy:1085
    old "Right Click"
    new "Rechtsklick"

    # screens.rpy:1089
    old "Mouse Wheel Up\nClick Rollback Side"
    new "Mausrad nach oben\nKlick Seite zurückblättern"

    # screens.rpy:1093
    old "Mouse Wheel Down"
    new "Mausrad nach unten"

    # screens.rpy:1100
    old "Right Trigger\nA/Bottom Button"
    new "Rechter Trigger\nA/Untere Taste"

    # screens.rpy:1104
    old "Left Trigger\nLeft Shoulder"
    new "Linker Trigger\nLinke Schulter"

    # screens.rpy:1108
    old "Right Shoulder"
    new "Rechte Schulter"

    # screens.rpy:1112
    old "D-Pad, Sticks"
    new "D-Pad, Sticks"

    # screens.rpy:1116
    old "Start, Guide"
    new "Start, Spieleberater"

    # screens.rpy:1120
    old "Y/Top Button"
    new "Y/Obere Taste"

    # screens.rpy:1123
    old "Calibrate"
    new "Kalibrierung"

    # screens.rpy:1188
    old "Yes"
    new "Ja"

    # screens.rpy:1189
    old "No"
    new "Nein"

    # screens.rpy:1235
    old "Skipping"
    new "Überspringen"

    # screens.rpy:1456
    old "Menu"
    new "Menü"

    # screens.rpy:1547
    old "Picture gallery"
    new "Bildergalerie"

    # screens.rpy:1548
    old "Music player"
    new "Music player"


# TODO: Translation updated at 2019-04-23 10:42

translate german strings:

    # screens.rpy:1532
    old "Musics and pictures"
    new "Musics and pictures"

    # screens.rpy:1541
    old "Opening song lyrics"
    new "Opening song lyrics"

    # screens.rpy:1545
    old "Characters profiles"
    new "Characters profiles"

    # screens.rpy:1566
    old "Bonus chapters"
    new "Bonus chapters"

    # screens.rpy:1569
    old "1 - A casual day at the club"
    new "1 - A casual day at the club"

    # screens.rpy:1574
    old "2 - Questioning sexuality (Sakura's route)"
    new "2 - Questioning sexuality (Sakura's route)"

    # screens.rpy:1579
    old "3 - Headline news"
    new "3 - Headline news"

    # screens.rpy:1584
    old "4a - A picnic at the summer (Sakura's route)"
    new "4a - A picnic at the summer (Sakura's route)"

    # screens.rpy:1589
    old "4b - A picnic at the summer (Rika's route)"
    new "4b - A picnic at the summer (Rika's route)"

    # screens.rpy:1594
    old "4c - A picnic at the summer (Nanami's route)"
    new "4c - A picnic at the summer (Nanami's route)"

    # screens.rpy:1611
    old "Max le Fou - Taichi's Theme"
    new "Max le Fou - Taichi's Theme"

    # screens.rpy:1613
    old "Max le Fou - Sakura's Waltz"
    new "Max le Fou - Sakura's Waltz"

    # screens.rpy:1615
    old "Max le Fou - Rika's theme"
    new "Max le Fou - Rika's theme"

    # screens.rpy:1617
    old "Max le Fou - Of Bytes and Sanshin"
    new "Max le Fou - Of Bytes and Sanshin"

    # screens.rpy:1619
    old "Max le Fou - Time for School"
    new "Max le Fou - Time for School"

    # screens.rpy:1621
    old "Max le Fou - Sakura's secret"
    new "Max le Fou - Sakura's secret"

    # screens.rpy:1623
    old "Max le Fou - I think I'm in love"
    new "Max le Fou - I think I'm in love"

    # screens.rpy:1625
    old "Max le Fou - Darkness of the Village"
    new "Max le Fou - Darkness of the Village"

    # screens.rpy:1627
    old "Max le Fou - Love always win"
    new "Max le Fou - Love always win"

    # screens.rpy:1629
    old "Max le Fou - Ondo"
    new "Max le Fou - Ondo"

    # screens.rpy:1631
    old "Max le Fou - Happily Ever After"
    new "Max le Fou - Happily Ever After"

    # screens.rpy:1633
    old "J.S. Bach - Air Orchestral suite #3"
    new "J.S. Bach - Air Orchestral suite #3"

    # screens.rpy:1635
    old "Mew Nekohime - TAKE MY HEART (TV Size)"
    new "Mew Nekohime - TAKE MY HEART (TV Size)"

# TODO: Translation updated at 2019-04-27 11:14

translate german strings:

    # screens.rpy:1552
    old "Achievements"
    new "Achievements"

